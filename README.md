Diese Webapp gibt Schimpfwörter aus. Geplante Features:

    - Zufallsfunktion
    - Tag-Suche

testuser: admin, merfherf

dependencies: python 3, django 1.10, pyyaml, pytz

git url: https://marneu@bitbucket.org/marneu/schimpf.git


TO DO:
    - unit-file für gunicorn schreiben
    - pfad in migration per fab-sed setzen
    - live-site bauen
