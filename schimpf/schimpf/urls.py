"""django_test_eigen URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^schimpf/', include('generator.urls')),
    url(r'^schimpf/admin/', admin.site.urls),
]
