from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django import forms
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.template import RequestContext
from .models import Word

import random

# for search
from django.db.models import Q

class IndexView(generic.ListView):
    template_name = 'generator/index.html'
    context_object_name = 'word_list'

    def get_queryset(self):
        return Word.random_word()

class SearchView(generic.ListView):
    template_name = 'generator/search.html'
    context_object_name = 'word_list'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            if len(query.split()) > 1:
                result = Word.objects.filter(
                            Q(tag__icontains=q) for q in query_list
                        ).order_by('word')
            else:
                result = Word.objects.filter(
                            Q(tags__icontains=query)
                        ).order_by('word')
        else:
            result = Word.objects.filter(
                        pub_date__lte=timezone.now()
                    ).order_by('word')[:80]
        return result
