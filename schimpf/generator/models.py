# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

#für Timesetter
import time
from datetime import datetime, timedelta
from pytz import timezone

import random

class Word(models.Model):
    word    = models.TextField(max_length=256)
    #author   = models.CharField(max_length=256)
    tags     = models.CharField(max_length=256)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.word
    def __str__(self):
        return self.word

    def save(self, *args, **kwargs):
        super(Word, self).save(*args, **kwargs)

    def random_word():
        words = Word.objects.all()
        index = random.randrange(words.count())
        return words[index]
