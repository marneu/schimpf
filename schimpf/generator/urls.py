from django.conf.urls import url

from . import views

app_name = 'generator'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^suche$', views.SearchView.as_view(), name="search"),
]
